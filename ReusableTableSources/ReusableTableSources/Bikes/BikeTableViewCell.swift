//
//  BikeTableViewCell.swift
//  ReusableTableSources
//
//  Created by Radi on 27.02.19.
//  Copyright © 2019 Radi. All rights reserved.
//

import UIKit

class BikeTableViewCell: UITableViewCell {

    @IBOutlet weak var bikeImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setup(for image: UIImage?) {
        bikeImageView.image = image
    }
}
