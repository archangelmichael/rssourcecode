//
//  BikesSectionTableViewDataSource.swift
//  ReusableTableSources
//
//  Created by Radi on 27.02.19.
//  Copyright © 2019 Radi. All rights reserved.
//

import UIKit

class BikesSectionTableViewDataSource: BaseTableViewSectionDataSource {
    var bikes: [UIImage?] = [
        UIImage.init(named: "r1"),
        UIImage.init(named: "r6")
    ]
    
    override init(tableView: UITableView,
                  didSelectRowAtIndex: rowSelectionCallback?) {
        super.init(tableView: tableView,
                   didSelectRowAtIndex: didSelectRowAtIndex)
        
        tableView.register(BikeTableViewCell.nib,
                           forCellReuseIdentifier: BikeTableViewCell.defaultReuseID)
    }
    
    override func itemAt(_ index: Int) -> AnyObject? {
        guard index < bikes.endIndex else {
            return nil
        }
        
        return bikes[index]
    }
    
    override func tableView(_ tableView: UITableView,
                            numberOfRowsInSection section: Int) -> Int {
        return bikes.endIndex
    }
    
    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        guard let bikeImage = itemAt(row) as? UIImage else {
            return getDefaultCellFor(tableView: tableView,
                                     atIndexPath:indexPath)
        }
        
        let bikeCell = tableView.dequeueReusableCell(withIdentifier: BikeTableViewCell.defaultReuseID,
                                                     for: indexPath) as! BikeTableViewCell
        bikeCell.setup(for: bikeImage)
        return bikeCell
    }
}
