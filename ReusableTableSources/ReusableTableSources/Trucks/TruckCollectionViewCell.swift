//
//  TruckCollectionViewCell.swift
//  ReusableTableSources
//
//  Created by Radi on 27.02.19.
//  Copyright © 2019 Radi. All rights reserved.
//

import UIKit

class TruckCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var truckImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
