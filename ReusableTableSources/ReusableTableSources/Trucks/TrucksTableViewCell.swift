//
//  TrucksTableViewCell.swift
//  ReusableTableSources
//
//  Created by Radi on 27.02.19.
//  Copyright © 2019 Radi. All rights reserved.
//

import UIKit

class TrucksTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var trucksDataSource = TrucksCollectionViewDataSource()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.register(TruckCollectionViewCell.nib,
                                forCellWithReuseIdentifier: TruckCollectionViewCell.defaultReuseID)
        collectionView.dataSource = trucksDataSource
        collectionView.delegate = trucksDataSource
        collectionView.reloadData()
    }
    
}
