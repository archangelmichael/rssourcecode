//
//  TrucksCollectionViewDataSource.swift
//  ReusableTableSources
//
//  Created by Radi on 27.02.19.
//  Copyright © 2019 Radi. All rights reserved.
//

import UIKit

class TrucksCollectionViewDataSource: NSObject, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var trucks: [UIImage?] = [
        UIImage.init(named: "r1"),
        UIImage.init(named: "r6"),
        UIImage.init(named: "r1"),
        UIImage.init(named: "r6"),
        UIImage.init(named: "r1"),
        UIImage.init(named: "r6"),
        UIImage.init(named: "r1"),
        UIImage.init(named: "r6"),
        UIImage.init(named: "r1"),
        UIImage.init(named: "r6"),
        UIImage.init(named: "r1"),
        UIImage.init(named: "r6"),
        UIImage.init(named: "r1"),
        UIImage.init(named: "r6"),
        UIImage.init(named: "r1"),
        UIImage.init(named: "r6"),
        UIImage.init(named: "r1"),
        UIImage.init(named: "r6"),
        UIImage.init(named: "r1"),
        UIImage.init(named: "r6")
    ]
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return trucks.endIndex
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let truckImage = trucks[indexPath.item]
        let truckCell = collectionView.dequeueReusableCell(withReuseIdentifier: TruckCollectionViewCell.defaultReuseID,
                                                           for: indexPath) as! TruckCollectionViewCell
        truckCell.truckImageView.image = truckImage
        return truckCell
    }
    
}
