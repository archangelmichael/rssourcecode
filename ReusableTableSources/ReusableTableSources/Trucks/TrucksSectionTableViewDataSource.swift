//
//  TrucksTableViewSectionDataSource.swift
//  ReusableTableSources
//
//  Created by Radi on 27.02.19.
//  Copyright © 2019 Radi. All rights reserved.
//

import UIKit

class TrucksSectionTableViewDataSource: BaseTableViewSectionDataSource {
   override init(tableView: UITableView,
                  didSelectRowAtIndex: rowSelectionCallback?) {
        super.init(tableView: tableView,
                   didSelectRowAtIndex: didSelectRowAtIndex)
        
        tableView.register(TrucksTableViewCell.nib,
                           forCellReuseIdentifier: TrucksTableViewCell.defaultReuseID)
        prepareForUpdate()
    }
    
    override func tableView(_ tableView: UITableView,
                            numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let trucksCell = tableView.dequeueReusableCell(withIdentifier: TrucksTableViewCell.defaultReuseID,
                                                     for: indexPath) as! TrucksTableViewCell
        return trucksCell
    }
}

