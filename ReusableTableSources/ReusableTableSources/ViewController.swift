//
//  ViewController.swift
//  ReusableTableSources
//
//  Created by Radi on 27.02.19.
//  Copyright © 2019 Radi. All rights reserved.
//

import UIKit

class ViewController: BaseTableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSections()
        tableView.reloadData()
    }

    func setupSections() {
        let selectCar: rowSelectionCallback = { (dataSource, itemIndex) in
            guard let carImage = dataSource.itemAt(itemIndex) as? UIImage else {
                return
            }
            
            print("CLICKED CAR AT : \(carImage)")
        }
        
        let selectBike: rowSelectionCallback = { (dataSource, itemIndex) in
            guard let carImage = dataSource.itemAt(itemIndex) as? UIImage else {
                return
            }
            
            print("CLICKED BIKE AT : \(carImage)")
        }
        
        sections = [
            CarsSectionTableViewDataSource(tableView: tableView,
                                           didSelectRowAtIndex: selectCar),
            TrucksSectionTableViewDataSource(tableView: tableView,
                                             didSelectRowAtIndex: nil),
            BikesSectionTableViewDataSource(tableView: tableView,
                                            didSelectRowAtIndex: selectBike),
        ]
    }
}

