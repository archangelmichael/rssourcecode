//
//  CarsSectionTableViewDataSource.swift
//  ReusableTableSources
//
//  Created by Radi on 27.02.19.
//  Copyright © 2019 Radi. All rights reserved.
//

import UIKit

class CarsSectionTableViewDataSource: BaseTableViewSectionDataSource {
    var cars: [UIImage?] = [
        UIImage.init(named: "e90"),
        UIImage.init(named: "f10")
    ]
    
    override init(tableView: UITableView,
                  didSelectRowAtIndex: rowSelectionCallback?) {
        super.init(tableView: tableView,
                   didSelectRowAtIndex: didSelectRowAtIndex)
        
        tableView.register(CarTableViewCell.nib,
                           forCellReuseIdentifier: CarTableViewCell.defaultReuseID)
    }
    
    override func itemAt(_ index: Int) -> AnyObject? {
        guard index < cars.endIndex else {
            return nil
        }
        
        return cars[index]
    }
    
    override func tableView(_ tableView: UITableView,
                            numberOfRowsInSection section: Int) -> Int {
        return cars.endIndex
    }
    
    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        guard let carImage = itemAt(row) as? UIImage else {
            return getDefaultCellFor(tableView: tableView,
                                     atIndexPath:indexPath)
        }
        
        let carCell = tableView.dequeueReusableCell(withIdentifier: CarTableViewCell.defaultReuseID,
                                                    for: indexPath) as! CarTableViewCell
        carCell.setup(for: carImage)
        return carCell
    }
}
