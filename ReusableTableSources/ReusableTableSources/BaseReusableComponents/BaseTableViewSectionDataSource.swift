//
//  ReusableTableViewDataSource.swift
//  ReusableTableSources
//
//  Created by Radi on 27.02.19.
//  Copyright © 2019 Radi. All rights reserved.
//

import UIKit

typealias rowSelectionCallback = ((BaseTableViewSectionDataSource, Int) -> Void)

class BaseTableViewSectionDataSource: NSObject {
    
    internal weak var tableView: UITableView?
    
    internal var sectionIndex: Int = 0
    var didSelectRow: rowSelectionCallback?
    
    init(tableView: UITableView,
         didSelectRowAtIndex: rowSelectionCallback?) {
        didSelectRow = didSelectRowAtIndex
        tableView.register(DefaultTableViewCell.nib,
                           forCellReuseIdentifier: DefaultTableViewCell.defaultReuseID)
    }
    
    func prepareForUpdate() { }
    
    func itemCount() -> Int {
        return 0
    }
    
    func itemAt(_ index: Int) -> AnyObject? {
        return nil
    }
    
    final func getDefaultCellFor(tableView: UITableView,
                                 atIndexPath: IndexPath) -> DefaultTableViewCell {
        let defaultCell = tableView.dequeueReusableCell(withIdentifier: DefaultTableViewCell.defaultReuseID,
                                                        for: atIndexPath) as! DefaultTableViewCell
        return defaultCell
    }
}

extension BaseTableViewSectionDataSource: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return getDefaultCellFor(tableView: tableView,
                                 atIndexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
}


extension BaseTableViewSectionDataSource: UITableViewDelegate {
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath) {
        didSelectRow?(self, indexPath.row)
    }
}
