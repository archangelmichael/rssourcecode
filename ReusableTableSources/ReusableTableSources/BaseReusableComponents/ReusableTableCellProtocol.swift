//
//  ReusableTableViewCell.swift
//  ReusableTableSources
//
//  Created by Radi on 27.02.19.
//  Copyright © 2019 Radi. All rights reserved.
//

import UIKit

protocol ReusableCell {
    static var defaultReuseID: String { get }
    static var estimatedRowHeight: CGFloat { get }
    static var nib: UINib { get }
}

extension ReusableCell where Self: UIView {
    static var defaultReuseID: String {
        return String(describing: self)
    }
    
    static var estimatedRowHeight: CGFloat {
        return 60.0
    }
    
    static var nib: UINib {
        return UINib(nibName: defaultReuseID,
                     bundle: Bundle.main)
    }
}

extension UITableViewCell: ReusableCell {
}

extension UICollectionViewCell: ReusableCell {
    
}
