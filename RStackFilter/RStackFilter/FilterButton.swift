//
//  FilterButton.swift
//  RStackFilter
//
//  Created by Radi on 19.08.19.
//  Copyright © 2019 Radi. All rights reserved.
//

import UIKit

class Filter {
    var key: String
    var value: String
    var active:  Bool
    
    init(key: String, value: String) {
        self.key = key
        self.value = value
        self.active = false
    }
}

protocol FilterButtonDelegate: class {
    func didSelect(filter: Filter)
}

class FilterButton: UIButton {
    
    var filter: Filter
    var normalColor: UIColor
    var activeColor: UIColor
    weak var filterDelegate: FilterButtonDelegate?

    required init(filter: Filter,
                  filterDelegate: FilterButtonDelegate?,
                  normalColor: UIColor = UIColor.red,
                  activeColor: UIColor = UIColor.blue,
                  contentInsets: UIEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)) {
        self.filter = filter
        self.normalColor = normalColor
        self.activeColor = activeColor
        super.init(frame: CGRect.zero)
        self.contentEdgeInsets = contentInsets
        self.layer.cornerRadius = 5
        self.layer.borderColor = UIColor.green.cgColor
        self.layer.borderWidth = 1
        self.clipsToBounds = true
        self.sizeToFit()
        self.addTarget(self, action: #selector(onFilterTap(sender:)), for: .touchUpInside)
        self.updateState()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateState() {
        self.setTitle(filter.value, for: .normal)
        let initialColor = filter.active ? activeColor :  normalColor
        let selectedColor = filter.active ? normalColor : activeColor
        self.setTitleColor(initialColor, for: .normal)
        self.setTitleColor(selectedColor, for: .highlighted)
        self.setTitleColor(selectedColor, for: .selected)
    }
    
    @objc private func onFilterTap(sender: Any) {
        filter.active = !filter.active
        updateState()
        filterDelegate?.didSelect(filter: filter)
    }
}
