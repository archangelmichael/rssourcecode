//
//  ViewController.swift
//  RStackFilter
//
//  Created by Radi on 19.08.19.
//  Copyright © 2019 Radi. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var scrollView: FilterScrollView!
    
    private let filters = [
        Filter(key: "dasdas", value: "adasdds"),
        Filter(key: "erwe", value: "adasdds"),
        Filter(key: "dssd", value: "adsasd ad asdasdds"),
        Filter(key: "fr", value: "adsaadasd  dsad assdds"),
        Filter(key: "rtre", value: "adsdds"),
        Filter(key: "kjllj", value: "adssdds"),
        Filter(key: "gfd", value: "fdeeww"),
        Filter(key: "xwsdw", value: "ads"),
        Filter(key: "cdee", value: "adsds"),
        Filter(key: "ngrgb", value: "dds"),
        Filter(key: "cdw", value: "adsasdds"),
        Filter(key: "xsw", value: "adsasda d asd adds"),
        Filter(key: "hyuj", value: "adsads"),
        Filter(key: "mjmh", value: "addds"),
        Filter(key: "das", value: "agasdds"),
        Filter(key: "ddas", value: "adsasdas d adsdds"),
        Filter(key: "mjewqww", value: "asdds"),
        Filter(key: "das", value: "ads"),
        Filter(key: "jujujy", value: "addds"),
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.setup(with: filters)
    }
}

