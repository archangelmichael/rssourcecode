//
//  FilterScrollView.swift
//  RStackFilter
//
//  Created by Radi on 19.08.19.
//  Copyright © 2019 Radi. All rights reserved.
//

import UIKit

class FilterScrollView: UIScrollView {
    var filters: [Filter] = []
    
    func setup(with filters: [Filter]) {
        self.filters = filters
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.spacing = 10
        stackView.distribution = .fill
        self.addSubview(stackView)
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            // Attaching the content's edges to the scroll view's edges
            stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            stackView.topAnchor.constraint(equalTo: self.topAnchor),
            stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            // Satisfying size constraints
            stackView.heightAnchor.constraint(equalTo: self.heightAnchor)
            ])
        
        addFilterButtons(for: filters,
                         in: stackView)
    }
    
    private func addFilterButtons(for filters: [Filter], in stackView: UIStackView) {
        for filter in filters {
            let button = FilterButton(filter: filter, filterDelegate: self)
//            button.addTarget(<#T##target: Any?##Any?#>, action: <#T##Selector#>, for: <#T##UIControl.Event#>)
            stackView.addArrangedSubview(button)
        }
    }
    
    fileprivate func updateFilters() {
        
    }
}

extension FilterScrollView: FilterButtonDelegate {
    func didSelect(filter: Filter) {
        // TODO: Update all filters
    }
}
