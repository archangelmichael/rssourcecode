//
//  CustomViewsViewController.swift
//  SizesCalculation
//
//  Created by Radi on 18.04.20.
//  Copyright © 2020 Radi. All rights reserved.
//

import UIKit

class CustomViewsViewController: UIViewController {

    @IBOutlet weak var stackView: UIStackView!
    
    private var items: [Item] = (0...20).map { _ in Item.random() }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initComplexTestViews(maxWidth: view.bounds.width)
    }
    
    private func initComplexTestViews(maxWidth: CGFloat) {
        print("Recalculate for \(maxWidth)")
        stackView.removeAllArrangedSubviews()
        
        items.forEach { (item: Item) in
            guard let testView: ComplexTestView = ComplexTestView.loadFromNib() else {
                fatalError("Could not instantiate view \(ComplexTestView.nibName)")
            }
            
            testView.updateUI(for: item)
            guard let exactHeight: CGFloat = testView.getHeight(for: maxWidth) else {
                fatalError("Could not calculate view height")
            }
            
            NSLayoutConstraint(item: testView,
                               attribute: .height,
                               relatedBy: .equal, toItem: nil,
                               attribute: .notAnAttribute,
                               multiplier: 1,
                               constant: exactHeight).isActive = true
            stackView.addArrangedSubview(testView)
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        initComplexTestViews(maxWidth: size.width)
    }
}
