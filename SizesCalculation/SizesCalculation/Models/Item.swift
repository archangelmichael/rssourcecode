//
//  Item.swift
//  SizesCalculation
//
//  Created by Radi on 18.04.20.
//  Copyright © 2020 Radi. All rights reserved.
//

import Foundation

class Item {
    var title: String?
    var code: String?
    var manufacturer: String?
    var specialty: String?
    
    static func random() -> Item {
        return Item(title: String.random(),
                    code: String.random(),
                    manufacturer: String.random(),
                    specialty: String.random())
    }
    
    init(title: String?,
         code: String?,
         manufacturer: String?,
         specialty: String?) {
        self.title = title
        self.code = code
        self.manufacturer = manufacturer
        self.specialty = specialty
    }
}
