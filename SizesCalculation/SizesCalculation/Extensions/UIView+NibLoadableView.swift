//
//  UIView+NibLoadable.swift
//  SizesCalculation
//
//  Created by Radi on 18.04.20.
//  Copyright © 2020 Radi. All rights reserved.
//

import UIKit

protocol NibLoadableView: class {
    static var nibName: String { get }
    static func loadFromNib<T: UIView>() -> T?
}

extension UIView {
    
    static var nibName: String {
        return String(describing: self)
    }
    
    static func loadFromNib<T: UIView>() -> T? {
        return Bundle.main.loadNibNamed(self.nibName,
                                        owner: self,
                                        options: .none)?.first as? T
    }
    
}
