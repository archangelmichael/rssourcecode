//
//  UIColor+Random.swift
//  SizesCalculation
//
//  Created by Radi on 18.04.20.
//  Copyright © 2020 Radi. All rights reserved.
//

import UIKit

extension UIColor {
    static func random() -> UIColor {
        return UIColor.init(red: CGFloat.random(in: 0...255) / 255,
                            green: CGFloat.random(in: 0...255) / 255,
                            blue: CGFloat.random(in: 0...255) / 255,
                            alpha: 1.0)
    }
}
