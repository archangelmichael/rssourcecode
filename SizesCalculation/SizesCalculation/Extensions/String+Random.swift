//
//  String+Random.swift
//  SizesCalculation
//
//  Created by Radi on 18.04.20.
//  Copyright © 2020 Radi. All rights reserved.
//

import UIKit

extension String {
    static func random(_ maxLenght: Int = 50) -> String? {
        let stringLenght = (0...maxLenght).randomElement() ?? 0
        let randomCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<stringLenght).compactMap { _ in randomCharacters.randomElement() })
    }
}
