//
//  UIView+Size.swift
//  SizesCalculation
//
//  Created by Radi on 18.04.20.
//  Copyright © 2020 Radi. All rights reserved.
//

import UIKit

extension UIView {
    func getHeight(for maxWidth: CGFloat) -> CGFloat? {
        let initialState = translatesAutoresizingMaskIntoConstraints
        let contentView = UIView(frame: CGRect(x: 0, y: 0, width: maxWidth, height: 1000))
        contentView.addFlexibleHeightConstrained(subview: self)
        
        contentView.layoutIfNeeded()
        let exactHeight = bounds.height
        print("View calculated height: ", exactHeight)
        removeFromSuperview()
        translatesAutoresizingMaskIntoConstraints = initialState
        return exactHeight
    }
    
    func addFlexibleHeightConstrained(subview: UIView) {
        addSubview(subview)
        subview.translatesAutoresizingMaskIntoConstraints = false
        subview.topAnchor.constraint(equalTo: topAnchor).isActive = true
        subview.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        subview.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
    }
}
