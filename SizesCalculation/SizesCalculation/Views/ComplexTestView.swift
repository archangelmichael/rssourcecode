//
//  ComplexTestView.swift
//  SizesCalculation
//
//  Created by Radi on 18.04.20.
//  Copyright © 2020 Radi. All rights reserved.
//

import UIKit

class ComplexTestView: UIView {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var codeTitleLabel: UILabel!
    @IBOutlet weak var codeLabel: UILabel!
    
    @IBOutlet weak var manufacturerTitleLabel: UILabel!
    @IBOutlet weak var manufacturerLabel: UILabel!
    
    @IBOutlet weak var specialtyTitleLabel: UILabel!
    @IBOutlet weak var specialtyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func updateUI(for item: Item) {
        backgroundColor = UIColor.random()
        titleLabel.text = item.title
        codeLabel.text = item.code
        manufacturerLabel.text = item.manufacturer
        specialtyLabel.text = item.specialty
    }
}
