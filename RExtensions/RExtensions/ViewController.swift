//
//  ViewController.swift
//  RExtensions
//
//  Created by Radi on 30.11.18.
//  Copyright © 2018 Radi. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.customPurple
    }

    @IBAction func onDownloadImage(_ sender: Any) {
        if let imageURL = URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR3f_Ojbitof6tKRsol_G7x4PFsL3PSF9V1ftbO7oIFRdxm2vAG") {
            let controller: ImagePreviewViewController = ImagePreviewViewController.loadFromNib()
            controller.imageURL = imageURL
            present(controller, animated: true, completion: nil)
        }
    }
    
}

