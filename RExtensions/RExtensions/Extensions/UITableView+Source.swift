//
//  UITableView+Source.swift
//  RExtensions
//
//  Created by Radi on 4.12.18.
//  Copyright © 2018 Radi. All rights reserved.
//

import UIKit

protocol TableSource: UITableViewDataSource, UITableViewDelegate { }

extension UITableView {
    func setup<T: TableSource>(source: T,
                               autoHeight: Bool = true,
                               estimatedHeigh: CGFloat = 80.0) {
        dataSource = source
        delegate = source
        if autoHeight {
            rowHeight = UITableView.automaticDimension
            self.estimatedRowHeight = estimatedHeigh
        }
    }
}

