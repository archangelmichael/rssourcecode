//
//  UIView+Reusable.swift
//  RExtensions
//
//  Created by Radi on 4.12.18.
//  Copyright © 2018 Radi. All rights reserved.
//

import UIKit

protocol ReusableView: class {
    static var defaultReuseIdentifier: String { get }
    static var defaultEstimatedHeight: CGFloat { get }
}

extension ReusableView where Self: UIView {
    static var defaultReuseIdentifier: String {
        return String(describing: self)
    }
    
    static var defaultEstimatedHeight: CGFloat {
        return 80.0
    }
}

extension UITableViewCell: ReusableView, NibLoadableView {
}

extension UICollectionViewCell: ReusableView, NibLoadableView {
}

extension UITableView {
    func register<T: UITableViewCell>(_: T.Type) { // where T: ReusableView
        register(T.self, forCellReuseIdentifier: T.defaultReuseIdentifier)
    }
    
    func registerNib<T: UITableViewCell>(_: T.Type) { // where T: ReusableView, T: NibLoadableView
        let bundle = Bundle(for: T.self)
        let nib = UINib(nibName: T.nibID, bundle: bundle)
        register(nib, forCellReuseIdentifier: T.defaultReuseIdentifier)
    }
    
    func dequeueReusableCell<T: UITableViewCell>(forIndexPath indexPath: IndexPath) -> T { // where T: ReusableView
        guard let cell = dequeueReusableCell(withIdentifier: T.defaultReuseIdentifier, for: indexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(T.defaultReuseIdentifier)")
        }
        
        return cell
    }
}

extension UICollectionView {
    func register<T: UICollectionViewCell>(_: T.Type) { // where T: ReusableView
        register(T.self, forCellWithReuseIdentifier: T.defaultReuseIdentifier)
    }
    
    func registerNib<T: UICollectionViewCell>(_: T.Type) { // where T: ReusableView, T: NibLoadableView
        let bundle = Bundle(for: T.self)
        let nib = UINib(nibName: T.nibID, bundle: bundle)
        register(nib, forCellWithReuseIdentifier: T.defaultReuseIdentifier)
    }
    
    func dequeueReusableCell<T: UICollectionViewCell>(forIndexPath indexPath: IndexPath) -> T { // where T: ReusableView
        guard let cell = dequeueReusableCell(withReuseIdentifier: T.defaultReuseIdentifier, for: indexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(T.defaultReuseIdentifier)")
        }
        
        return cell
    }
}
