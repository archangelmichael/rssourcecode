//
//  UIViewController+NibLoadable.swift
//  RExtensions
//
//  Created by Radi on 30.11.18.
//  Copyright © 2018 Radi. All rights reserved.
//

import UIKit

protocol NibLoadableView {
    static var nibID: String { get }
}

extension NibLoadableView where Self: UIView {
    static var nibID: String { return String(describing: self) }
}

extension UIViewController: NibLoadableView {
    static var nibID: String { return String(describing: self) }
    
    class func loadFromNib<T: UIViewController>() -> T {
        return T(nibName: self.nibID, bundle: nil)
    }
}
