//
//  ImagePreviewViewController.swift
//  RExtensions
//
//  Created by Radi on 30.11.18.
//  Copyright © 2018 Radi. All rights reserved.
//

import UIKit

class ImagePreviewViewController: UIViewController {
    
    @IBOutlet weak var previewScrollView: UIScrollView!
    @IBOutlet weak var previewImageView: UIImageView!
    
    private let minZoomScale: CGFloat = 1.0
    private let maxZoomScale: CGFloat = 10.0
    private let doubleTapMultiplier: CGFloat = 3.0
    
    var image: UIImage?
    var imageURL: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupScrollView()
        if let image = image {
            previewImageView.image = image
        }
        else if let imageURL = imageURL {
            URLSession.shared.dataTask(with: imageURL) { [weak self] (imageData, response, error) in
                if let imageData = imageData,
                    let downloadedImage = UIImage(data: imageData) {
                    DispatchQueue.main.async { [weak self] in
                        self?.previewImageView.image = downloadedImage
                    }
                }
            }.resume()
        }
    }
    
    private func setupScrollView() {
        previewScrollView.isHidden = false
        previewScrollView.delegate = self
        previewScrollView.minimumZoomScale = minZoomScale
        previewScrollView.maximumZoomScale = maxZoomScale
        previewScrollView.zoomScale = 1.0
        
        let singleTap = UITapGestureRecognizer(target: self,
                                               action: #selector(onSingleTap(gesture:)))
        singleTap.numberOfTapsRequired = 1
        previewScrollView.gestureRecognizers?.append(singleTap)
        
        let doubleTap = UITapGestureRecognizer(target: self,
                                               action: #selector(onDoubleTap(gesture:)))
        doubleTap.numberOfTapsRequired = 2
        previewScrollView.gestureRecognizers?.append(doubleTap)
        
        singleTap.require(toFail: doubleTap)
    }
    
    @objc func onSingleTap(gesture: UIGestureRecognizer) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func onDoubleTap(gesture: UIGestureRecognizer) {
        var newZoomLevel = previewScrollView.zoomScale
        if previewScrollView.zoomScale > previewScrollView.minimumZoomScale {
            newZoomLevel = previewScrollView.minimumZoomScale
            
        }
        else {
            newZoomLevel = previewScrollView.zoomScale * doubleTapMultiplier
            if newZoomLevel > previewScrollView.maximumZoomScale {
                newZoomLevel = previewScrollView.maximumZoomScale
            }
        }
        
        previewScrollView.setZoomScale(newZoomLevel, animated: true)
    }
}

extension ImagePreviewViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return previewImageView
    }
}
